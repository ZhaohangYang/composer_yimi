<?php

namespace ZhaohangYang\ToolsPhpYimi;

use ZhaohangYang\ToolsPhpYimi\YimiBasic;

class YimiCall extends YimiBasic
{
    // 呼叫
    public static function callOut($yimi_item)
    {
        if (1 == $yimi_item['type']) {
            // 双向手机呼叫
            return self::phoneCallOut($yimi_item);
        } else {
            // 单向pc呼叫
            return self::pcCallOut($yimi_item);
        }
    }
    // 双向手机呼叫
    public static function phoneCallOut($yimi_item)
    {
        $body = [
            'callBack' => [
                'appId'    => self::$yimiParams['app_id'],
                'to'       => $yimi_item['to'],
                'from'     => $yimi_item['phone'],
                'userData' => $yimi_item['user_data'],
            ],
        ];
        $url = '/' . self::$yimiParams['version'] . '/SubAccounts/' . self::$yimiParams['sub_account_sid'] . '/Calls/callBack?sig=' . self::sigParameter();
        return self::sendResquest($url, $body);
    }
    // 单向pc呼叫
    public static function pcCallOut($yimi_item)
    {
        self::signIn($yimi_item);
        $body = [
            'callOut' => [
                'appId'      => self::$yimiParams['app_id'],
                'to'         => $yimi_item['to'],
                'workNumber' => $yimi_item['seats'],
                'userData'   => $yimi_item['user_data'],
            ],
        ];
        $url = '/' . self::$yimiParams['version'] . '/SubAccounts/' . self::$yimiParams['sub_account_sid'] . '/CallCenter/callOut?sig=' . self::sigParameter();
        return self::sendResquest($url, $body);
    }
    // 迁入
    public static function signIn($item)
    {
        $body = [
            'signIn' => [
                'appId'      => self::$yimiParams['app_id'],
                'workNumber' => $item['seats'],
            ],
        ];

        $url = '/' . self::$yimiParams['version'] . '/SubAccounts/' . self::$yimiParams['sub_account_sid'] . '/CallCenter/signIn?sig=' . self::sigParameter();
        return self::sendResquest($url, $body);
    }
}
