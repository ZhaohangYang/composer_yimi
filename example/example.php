<?php

require '../vendor/autoload.php';
use ZhaohangYang\ToolsPhpYimi\YimiBasic;
use ZhaohangYang\ToolsPhpYimi\YimiCall;

$yimi_config = [
    'account_sid'     => '主账号id',
    'auth_token'      => '主账号tocken',
    'sub_account_sid' => '子账号id',
    'sub_auth_token'  => '子账号tocken',
    'app_id'          => '应用id',
    'version'         => '20180319',
    'is_test'         => true,
];
YimiBasic::init($yimi_config);

$yimi_item = [
    'to'         => '要拨出的电话',
    'from'       => '双向回拨绑定的手机号',
    'workNumber' => '绑定的坐席号',
    'userData'   => '需要回传携带的信息',
];
YimiCall::callOut($yimi_item);
